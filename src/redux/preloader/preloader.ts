export enum PreloaderActionTypes {
    SetPreloader = "SET_PRELOADER"
}

export type SetPreloaderAction = {
    type: PreloaderActionTypes.SetPreloader,
    payload: boolean
}

export function setPreloader(value: boolean): SetPreloaderAction {
    return {
        type: PreloaderActionTypes.SetPreloader,
        payload: value
    };
}

export type PreloaderAction = SetPreloaderAction;

export function preloaderReducer(state: boolean = true, action: PreloaderAction) {
    switch (action.type) {
        case PreloaderActionTypes.SetPreloader:
            return action.payload;
        default:
            return state;
    }
}

import {EditModalAction, EditModalActionTypes} from "./editModalActions";

export type EditModalState = boolean;

const initialState: EditModalState = false;

export function editModalReducer(state: EditModalState = initialState, action: EditModalAction): EditModalState {
    switch (action.type) {
        case EditModalActionTypes.ShowModal:
            return true;
        case EditModalActionTypes.HideModal:
            return false;
        default:
            return state;
    }
}

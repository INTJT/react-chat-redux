export enum EditModalActionTypes {
    ShowModal = "SHOW_MODAL",
    HideModal = "HIDE_MODAL"
}

export type ShowModalAction = { type: EditModalActionTypes.ShowModal }

export type HideModalAction = { type: EditModalActionTypes.HideModal }

export type EditModalAction = ShowModalAction | HideModalAction;

import {UserModel} from "../../models/userModel";
import {AddUserAction, DeleteUserAction, SetUsersAction, UsersActionTypes} from "./usersActions";

export function setUsers(users: UserModel[]): SetUsersAction {
    return {
        type: UsersActionTypes.SetUsers,
        payload: users
    }
}

export function addUser(user: UserModel): AddUserAction {
    return {
        type: UsersActionTypes.AddUser,
        payload: user
    }
}

export function deleteUser(user: UserModel | string): DeleteUserAction {
    return {
        type: UsersActionTypes.DeleteUser,
        payload: user
    }
}

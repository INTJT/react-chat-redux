import {combineReducers, createStore} from "redux";
import {MessageModel} from "../models/messageModel";
import {UserModel} from "../models/userModel";
import {editModalReducer} from "./edit-modal/editModalReducer";
import {messagesReducer} from "./messages/messagesReducer";
import {usersReducer} from "./users/usersReducer";
import {preloaderReducer} from "./preloader/preloader";

export type StoreState = {
    chat: {
        messages: MessageModel[],
        users: Map<string, UserModel>,
        editModal: boolean,
        preloader: boolean
    }
}

export const rootReducer = combineReducers({
    chat: combineReducers({
        messages: messagesReducer,
        users: usersReducer,
        editModal: editModalReducer,
        preloader: preloaderReducer
    })
});

export const store = createStore(rootReducer);

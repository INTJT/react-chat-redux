import {MessageModel} from "../../models/messageModel";

export enum MessagesActionTypes {
    SetMessages = "SET_MESSAGES",
    AddMessage = "ADD_MESSAGE",
    ReplaceMessage = "REPLACE_MESSAGE",
    DeleteMessage = "DELETE_MESSAGE"
}

export type SetMessageAction = {
    type: MessagesActionTypes.SetMessages,
    payload: MessageModel[]
}

export type AddMessageAction = {
    type: MessagesActionTypes.AddMessage,
    payload: MessageModel
}

export type ReplaceMessageAction = {
    type: MessagesActionTypes.ReplaceMessage,
    payload: {
        old: MessageModel | string,
        new: MessageModel
    }
}

export type DeleteMessageAction = {
    type: MessagesActionTypes.DeleteMessage,
    payload: MessageModel | string
}

export type MessagesAction = SetMessageAction | AddMessageAction | ReplaceMessageAction | DeleteMessageAction

export interface UserModel {
    id: string,
    name: string,
    avatar: string,
}

export const ME: UserModel = {
    id: "5328dba1-1b8f-11e8-9629-11037",
    name: "XXX",
    avatar: "https://pbs.twimg.com/profile_images/1210618202457292802/lt9KD2lt.jpg"
}

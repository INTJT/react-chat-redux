import "./Preloader.css";

interface PreloaderProps {
    count: number,
    duration: number,
    waves: number
}

export function Preloader(props: PreloaderProps) {

    const range = Array.from(new Array(props.count).keys());

    return (
        <div className="preloader">
            <div className="preloader-main" style={{
                animationDuration: `${props.duration}s`
            }}/>
            {range.map(i => {

                const angle = 2 * Math.PI / props.count * i;

                return (
                    <div className="preloader-point" key={i} style={{
                        top: `${100 * (1 + Math.sin(angle)) / 2}%`,
                        left: `${100 * (1 + Math.cos(angle)) / 2}%`,
                        animationDelay: `-${props.duration * i / props.count}s`,
                        animationDuration: `${props.duration / props.waves}s`
                    }}/>
                );

            })}
        </div>
    );

}

Preloader.defaultProps = {
    count: 21,
    duration: 1.5,
    waves: 3
}

import {Chat} from "../chat/Chat";
import {Header} from "./header/Header";
import './App.css';
import {Provider} from "react-redux";
import {store} from "../../redux/store";

function App() {
  return (
      <Provider store={store}>
          <div className="app">
              <Header />
              <Chat />
          </div>
      </Provider>
  );
}

export default App;

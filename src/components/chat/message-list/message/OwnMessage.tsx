import moment from "moment";
import editIcon from "../../../../icons/edit.svg";
import deleteIcon from "../../../../icons/delete.svg";
import {useDispatch, useSelector} from "react-redux";
import {showModal} from "../../../../redux/edit-modal/editModalCreators";
import {deleteMessage} from "../../../../redux/messages/messagesCreators";
import {deleteUser} from "../../../../redux/users/usersCreators";
import {MessageModel} from "../../../../models/messageModel";
import {StoreState} from "../../../../redux/store";
import {ME} from "../../../../models/userModel";

export interface OwnMessageProps {
    message: MessageModel
}

export function OwnMessage(props: OwnMessageProps) {

    const state = useSelector((state: StoreState) => ({
        canEdit: state.chat.messages[state.chat.messages.length - 1] === props.message && props.message.senderId === ME.id,
        myMessagesCount: state.chat.messages.filter(message => message.senderId === ME.id).length
    }));
    const dispatch = useDispatch();

    return (
        <div className="own-message">
            <div className="message-buttons">
                {
                    state.canEdit ? (
                        <button
                            className="message-edit"
                            onClick={() => dispatch(showModal())}
                        >
                            <img className="icon" src={editIcon} width={15} height={15} alt="edit"/>
                        </button>
                    ): null
                }
                <button
                    className="message-delete"
                    onClick={() => {
                        dispatch(deleteMessage(props.message.id));
                        if(state.myMessagesCount === 1) dispatch(deleteUser(ME));
                    }}
                >
                    <img className="icon" src={deleteIcon} width={15} height={15} alt="edit"/>
                </button>
            </div>
            <div className="message-body">
                <div className="message-text">{props.message.text}</div>
                { props.message.editedAt ? <div className="message-edited one-line">edited</div> : null }
                <time className="message-time one-line">{moment(props.message.createdAt).format("HH:mm")}</time>
            </div>
        </div>
    );

}

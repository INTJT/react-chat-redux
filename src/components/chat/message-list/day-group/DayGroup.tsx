import {MessageModel} from "../../../../models/messageModel";
import moment from "moment";
import {ME, UserModel} from "../../../../models/userModel";
import {Message} from "../message/Message";
import {OwnMessage} from "../message/OwnMessage";
import "./DayGroup.css";
import {useSelector} from "react-redux";
import {StoreState} from "../../../../redux/store";

interface MessageDividerProps {
    day: Date,
    messages: MessageModel[],
}

function dateFormat(date: Date): string {
    const momentDate = moment(date);
    const today = moment();
    if(momentDate.isSame(today, "day")) return "Today";
    if(momentDate.isSame(today.clone().subtract(1, "day").startOf("day"))) return "Yesterday";
    else return momentDate.format("dddd, DD MMMM");
}

export function DayGroup(props: MessageDividerProps) {

    const users = useSelector((state: StoreState) => state.chat.users);

    return (
        <div className="day-group">
            <div className="messages-divider">{dateFormat(props.day)}</div>
            {
                props.messages.map(message => {
                    if (message.senderId === ME.id) {
                        return (
                            <OwnMessage
                                key = {message.id}
                                message={message}
                            />
                        );
                    }
                    else {
                        return (
                            <Message
                                key = {message.id}
                                message={message}
                                sender={users.get(message.senderId) as UserModel}
                            />
                        )
                    }
                })
            }
        </div>
    );

}

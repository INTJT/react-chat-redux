import moment from "moment";
import usersIcon from "../../../icons/users.svg";
import messageIcon from "../../../icons/message.svg";
import "./Header.css";
import {useSelector} from "react-redux";
import {StoreState} from "../../../redux/store";

export function Header() {

    const state = useSelector((state: StoreState) => ({
        messagesCount: state.chat.messages.length,
        usersCount: state.chat.users.size,
        lastMessage: state.chat.messages[state.chat.messages.length - 1]
    }));

    return (
        <div className="header">
            <h1 className="header-title one-line">Chat</h1>
            <div className="one-line header-users">
                <img className="icon" width={20} height={20} src={usersIcon} alt=""/>
                <span className="header-users-count">{state.usersCount}</span> members
            </div>
            <div className="one-line header-messages">
                <img className="icon" width={20} height={20} src={messageIcon} alt=""/>
                <span className="header-messages-count">{state.messagesCount}</span> messages
            </div>
            {state.lastMessage ? (
                <div className="one-line header-date">
                    last message
                    at <time className="header-last-message-date">{moment(state.lastMessage.createdAt).format("DD.MM.yyyy HH:mm")}</time>
                </div>
            ) : null}
        </div>
    );

}
